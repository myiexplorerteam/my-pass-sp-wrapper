# SMS-Wrapper ( Laravel 5 Package )

## Installation

Just add

    "repositories": [
      {
        "type": "vcs",
        "url":  "git@bitbucket.org:innoractivehackers/my-pass-sp-wrapper.git"
      }
    ],

to your composer.json. Then run the `composer require` command from your terminal:

    composer require innoractive/my-pass-sp-wrapper:dev-master

Then in your `config/app.php` add
```php
    Innoractive\SMSWrapper\MyPassSPWrapperProvider::class,
```
in the `providers` array.

Then run:

    php artisan vendor:publish --provider=Innoractive\MyPassSPWrapper\MyPassSPWrapperProvider
    
## Config

config/saml2_settings.php

$idp_host

##### Laravel 5.2 only
'routesMiddleware' => [
    'session_only'
],

config/mypass_settings.php

'userModel' => 'App\User',

##### Laravel 5.2 only
'routesMiddleware' => [
    'session_only'
],

## Migration

2017_10_16_000000_create_table_my_pass.php

## App/Http/Kernel.php

'session_only' => [
    \App\Http\Middleware\EncryptCookies::class,
    \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
    \Illuminate\Session\Middleware\StartSession::class,
],

## TODO
