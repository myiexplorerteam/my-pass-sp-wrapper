<?php

Route::group(['prefix' => config('mypass_settings.routesPrefix'), 'middleware' => config('mypass_settings.routesMiddleware'), 'namespace' => 'Innoractive\MyPassSPWrapper\Controllers'], function (){
    Route::get('login', 'Saml2Controller@login')->name('login');
    Route::any('logout', 'Saml2Controller@logout')->name('logout');
});