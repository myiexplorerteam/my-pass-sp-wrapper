<?php

namespace Innoractive\MyPassSPWrapper;

/**
 * laravel service provider
 */

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Innoractive\MyPassSPWrapper\Commands\BusQueueListen;

class MyPassSPWrapperProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        if(config('mypass_settings.useRoutes', false) == true ) {
            include __DIR__ . '/route.php';
        }

        $this->publishes([
            __DIR__.'/database/migrations/2017_10_16_000000_create_table_my_pass.php' => database_path('migrations') . '/2017_10_16_000000_create_table_my_pass.php',
            __DIR__.'/config/saml2_settings.php' => config_path('saml2_settings.php'),
            __DIR__.'/config/mypass_settings.php' => config_path('mypass_settings.php'),
            __DIR__.'/config/tail-settings.php' => config_path('tail-settings.php')
        ]);

        $this->eventListen();
	}

	protected function eventListen(){
        Event::listen('Aacotroneo\Saml2\Events\Saml2LoginEvent', 'Innoractive\MyPassSPWrapper\Listeners\Saml2LoginListener');
        Event::listen('Aacotroneo\Saml2\Events\Saml2LogoutEvent', 'Innoractive\MyPassSPWrapper\Listeners\Saml2LogoutListener');

        Event::listen('Innoractive\MyPassSPWrapper\Events\BusMyPassUpdatedEvent', 'Innoractive\MyPassSPWrapper\Listeners\BusMyPassUpdatedListener');
        Event::listen('Innoractive\MyPassSPWrapper\Events\BusMyPassCreatedEvent', 'Innoractive\MyPassSPWrapper\Listeners\BusMyPassCreatedListener');
        Event::listen('Innoractive\MyPassSPWrapper\Events\BusMyPassDeletedEvent', 'Innoractive\MyPassSPWrapper\Listeners\BusMyPassDeletedListener');
    }

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
	    // register saml2
        $this->app->register('Aacotroneo\Saml2\Saml2ServiceProvider');

        // register rabbitmq
        $this->app->register('Mookofe\Tail\ServiceProvider');
        AliasLoader::getInstance()->alias('Tail', 'Mookofe\Tail\Facades\Tail');

        // register command
        $this->commands([
            BusQueueListen::class
        ]);

        // bind service
        $this->app->bind('my-pass-sp-wrappper', function() {
            return new MyPassSPWrapper(config('saml2_settings.idpHost'));
        });
    }
}
