<?php

namespace Innoractive\MyPassSPWrapper\Listeners;

use Aacotroneo\Saml2\Events\Saml2LogoutEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class Saml2LogoutListener
{
    public function handle(Saml2LogoutEvent $event){
        Log::debug('event - logout');
        Log::debug(session()->getId());
        Auth::logout();
        Log::debug(session()->getId());
        Session::save();
    }
}
