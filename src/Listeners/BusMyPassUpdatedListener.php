<?php

namespace Innoractive\MyPassSPWrapper\Listeners;

use Innoractive\MyPassSPWrapper\Events\BusMyPassUpdatedEvent;
use Innoractive\MyPassSPWrapper\Events\MyPassUpdatedEvent;
use Innoractive\MyPassSPWrapper\Models\MyPass;
use DB;

class BusMyPassUpdatedListener
{
    public function handle(BusMyPassUpdatedEvent $event){
        $jsonUser = $event->jsonUser;

        // check mypass
        $myPass = MyPass::searchUid($jsonUser->uid)->first();
        if (!is_null($myPass) && ($jsonUser->profile_updated_at > $myPass->updated_at->timestamp)){
            DB::transaction(function () use ($jsonUser, &$myPass) {
//                $myPass->user->update([
//                    'name' => $jsonUser->name,
//                    'email' => $jsonUser->email,
//                    'mobile' => $jsonUser->mobile
//                ]);
                $myPass->updated_at = $jsonUser->profile_updated_at;
                $myPass->save();

                // due to 5.2 model event ...
                event(new MyPassUpdatedEvent($myPass, $jsonUser));
            });
        }
    }
}
