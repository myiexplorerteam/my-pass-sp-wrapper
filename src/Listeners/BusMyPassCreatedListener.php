<?php

namespace Innoractive\MyPassSPWrapper\Listeners;

use Innoractive\MyPassSPWrapper\Events\BusMyPassCreatedEvent;
use Innoractive\MyPassSPWrapper\Events\MyPassCreatedEvent;
use Innoractive\MyPassSPWrapper\Models\MyPass;
use DB;

class BusMyPassCreatedListener
{
    public function handle(BusMyPassCreatedEvent $event){
        $jsonUser = $event->jsonUser;

        // check mypass
        $myPass = MyPass::searchUid($jsonUser->uid)->first();

        // if not found
        if (is_null($myPass)){
            DB::transaction(function () use ($jsonUser) {
                // create mypass
                $myPass = MyPass::create([
                    'uid' => $jsonUser->uid,
                    'updated_at' => $jsonUser->profile_updated_at
                ]);

                // due to 5.2 model event ...
                event(new MyPassCreatedEvent($myPass, $jsonUser));
            });
        }
    }
}
