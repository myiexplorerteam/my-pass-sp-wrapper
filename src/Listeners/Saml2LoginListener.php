<?php

namespace Innoractive\MyPassSPWrapper\Listeners;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Innoractive\MyPassSPWrapper\Models\MyPass;

class Saml2LoginListener
{
    protected $uid;
    protected $profileUpdateAt;
    protected $group;
    protected $myPass;

    protected function initUserData(Saml2LoginEvent $event){
        $user = $event->getSaml2User();
//        $userData = [
//            'id' => $user->getUserId(),
//            'attributes' => $user->getAttributes(),
////                'assertion' => $user->getRawSamlAssertion()
//        ];

        $this->uid = $user->getUserId();
        $this->profileUpdateAt = $user->getAttribute('profile_updated_at')[0];
        $this->group = $user->getAttribute('http://schemas.xmlsoap.org/claims/Group')[0];

        // check mypass
        $this->myPass = MyPass::searchUid($this->uid)->first();
    }

    public function handle(Saml2LoginEvent $event){
        $this->initUserData($event);

        // generate user data array
        $userDataArray = $this->getUserDataArray($this->uid);

        if (is_null($this->myPass)){
            DB::transaction(function () use ($userDataArray) {
                // create user
                $user = $this->createUser($userDataArray);

                // create mypass
                $this->createMyPass($user->id, $this->uid);
            });
        }else{
            // check updated_at
            if ($this->isUpdateRequired()){
                $this->updateMyPass($userDataArray);
            }
        }

        $this->login();
    }

    /** Overrideable */
    protected function login(){
        Auth::login($this->myPass->user);
    }

    /** Overrideable */
    protected function createUser($userDataArray){
        // create user
        return app(config('mypass_settings.userModel'))::create($userDataArray);
    }

    /** Overrideable */
    protected function getUserDataArray($uid){
        $json = $this->getUserJson($uid);

        return [
            'name' => $json->name,
            'email' => $json->email,
            'mobile' => $json->mobile,
        ];
    }

    /** Overrideable */
    protected function getUserJson($uid){
        // call api
        $url = config('saml2_settings.idpHost') . 'api/user/' . $uid;

        $content = file_get_contents($url);

        return json_decode($content);
    }

    protected function isUpdateRequired(){
        return $this->profileUpdateAt > $this->myPass->updated_at->timestamp;
    }

    protected function createMyPass($user_id, $uid){
        // create mypass
        $this->myPass = MyPass::create([
            'user_id' => $user_id,
            'uid' => $uid
        ]);
    }
    
    protected function updateMyPass($userDataArray){
        $this->updateUser($userDataArray);
        $this->myPass->updated_at = $this->profileUpdateAt;
        $this->myPass->save();
    }

    protected function updateUser($userDataArray){
        $this->myPass->user->update($userDataArray);
    }
}
