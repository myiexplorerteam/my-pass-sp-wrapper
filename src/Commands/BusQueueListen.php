<?php

namespace Innoractive\MyPassSPWrapper\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Innoractive\MyPassSPWrapper\Events\BusMyPassCreatedEvent;
use Innoractive\MyPassSPWrapper\Events\BusMyPassDeletedEvent;
use Innoractive\MyPassSPWrapper\Events\BusMyPassUpdatedEvent;
use Innoractive\MyPassSPWrapper\Models\MyPass;


class BusQueueListen extends Command
{
    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_DELETE = 'delete';

    protected $signature = 'mypass:bus_queue_listen';

    protected $description = 'MyPass Bus Queue Listen';

    protected $type;
    protected $content;
    protected $action;

    public function handle()
    {
        \Tail::listen(config('mypass_settings.identifier'), function ($message) {
            // show message
            $this->info($message);

            try{
                // convert message to json
                $json = json_decode($message);

                $this->type = $json->type;
                $this->content = $json->content;
                $this->action = $json->action;

                $this->process();
            }catch (\Exception $e){
                $this->info($e->getMessage());
            }
        });
    }

    private function process(){
        if ($this->type == 'user'){
            $this->user();
        }else{
            $this->others();
        }
    }

    protected function others(){}

    private function user(){
        switch($this->action){
            case self::ACTION_CREATE:
                event(new BusMyPassCreatedEvent($this->content));
                break;
            case self::ACTION_UPDATE:
                event(new BusMyPassUpdatedEvent($this->content));
                break;
            case self::ACTION_DELETE:
                event(new BusMyPassDeletedEvent($this->content));
                break;
        }
    }
}
