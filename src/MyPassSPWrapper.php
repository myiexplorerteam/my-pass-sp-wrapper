<?php

namespace Innoractive\MyPassSPWrapper;

use Innoractive\MyPassSPWrapper\Classes\HttpPostRequest;

class MyPassSPWrapper {
    protected $server;

    /**
     * MyPassSPWrapper constructor.
     * @param $server
     */
    public function __construct($server) {
        $this->server = $server;
    }

    public function createUser() {
        // create user
        $httpPostRequest = new HttpPostRequest($this->server . 'api/user/create', []);

        if (!$httpPostRequest->isError()){
           // get user info
        }else{
            // error
//            echo $httpPostRequest->getResponseStatusCode();
//            echo $httpPostRequest->getResponseErrorCode();
//            echo $httpPostRequest->getResponseErrorMessage();

//            throw new \Exception($httpPostRequest->getResponseErrorMessage());
        }

        return null;
    }
}