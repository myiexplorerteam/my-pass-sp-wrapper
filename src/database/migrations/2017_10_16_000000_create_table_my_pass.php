<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMyPass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_passes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uid')->unique();
            $table->integer('user_id')->unsigned();

            $table->integer('created_at')->index();
            $table->integer('updated_at')->index();
            $table->softDeletes();

            // FK
//            $table->foreign('user_id')->references('id')->on('users')
//                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_passes');
    }
}
