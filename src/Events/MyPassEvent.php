<?php

namespace Innoractive\MyPassSPWrapper\Events;

class MyPassEvent
{
    public $myPass;
    public $jsonUser;

    /**
     * MyPassEvent constructor.
     * @param $myPass
     * @param $jsonUser
     */
    public function __construct($myPass, $jsonUser = null) {
        $this->myPass = $myPass;
        $this->jsonUser = $jsonUser;
    }
}
