<?php

namespace Innoractive\MyPassSPWrapper\Events;

class MyPassDeletedEvent
{
    protected $uid;

    /**
     * MyPassDeletedEvent constructor.
     * @param $uid
     */
    public function __construct($uid) {
        $this->uid = $uid;
    }
}