<?php

return [
    'identifier' => env('APP_URL', ''),

    'routesPrefix' => '',

    /**
     * If 'useRoutes' is set to true, the package defines five new routes:
     *
     *    Method | URI                      | Name
     *    -------|--------------------------|------------------
     *    GET    | {routesPrefix}/login     | login
     *    ANY    | {routesPrefix}/logout    | logout
     */
    'useRoutes' => true,

    /**
     * which middleware group to use for the saml routes
     * Laravel 5.2 will need a group which includes StartSession
     */
    'routesMiddleware' => [],

    /** Replace if not same */
    'userModel' => 'App\User',
];
